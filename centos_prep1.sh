
#!/bin/bash
mkdir /var/lib/postgresql/data -p
nmcli device
nmcli connection modify ens160 ipv4.addresses 192.168.31.159/24
nmcli connection modify ens160 ipv4.gateway 192.168.31.2
yum check-update -y && yum update kernel -y && yum update -y
yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin nano -y
systemctl start docker
docker network create -d bridge externallconn --subnet 192.168.133.0/24 --ip-range 192.168.133.0/25
systemctl enable /usr/lib/systemd/system/docker.service
# systemctl enable docker.service
docker run hello-world
